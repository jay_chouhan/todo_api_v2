const Sequelize = require('sequelize');
const db = require('../db');

const Subtasks = db.define(
  'subtasks',
  {
    subtask: {
      type: Sequelize.STRING,
    },
    subtask_status: {
      type: Sequelize.BOOLEAN,
    },
    task_id: {
      type: Sequelize.INTEGER,
    },
  },
  {
    timestamps: false,
  }
);

module.exports = Subtasks;
