const Sequelize = require('sequelize');
const db = require('../db');
const SubTasks = require('./Subtasks');
const Users = require('./Tasks');

const Tasks = db.define(
  'tasks',
  {
    task: {
      type: Sequelize.STRING,
    },
    task_status: {
      type: Sequelize.BOOLEAN,
    },
    user_id: {
      type: Sequelize.INTEGER,
    },
  },
  {
    timestamps: false,
  }
);

Tasks.hasMany(SubTasks, { foreignKey: 'task_id' });
SubTasks.belongsTo(Tasks, { foreignKey: 'task_id' });

module.exports = Tasks;
