const Sequelize = require('sequelize');
const db = require('../db');
const RefreshTokens = require('./Refresh-tokens');
const Tasks = require('./Tasks');

const Users = db.define(
  'users',
  {
    username: {
      type: Sequelize.STRING,
    },
    password: {
      type: Sequelize.STRING,
    },
  },
  { timestamps: false }
);

Users.hasMany(Tasks, { foreignKey: 'user_id' });
Users.hasOne(RefreshTokens, { foreignKey: 'user_id' });

module.exports = Users;
