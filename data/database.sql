

CREATE DATABASE api_data;


CREATE TABLE api_data.users (
  `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(150) UNIQUE NOT NULL ,
  `password` VARCHAR(255) NOT NULL
);

CREATE TABLE api_data.tasks (
  `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `task` VARCHAR(150) NOT NULL,
  `task_status` BOOLEAN,
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE api_data.subtasks (
  `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `task_id` INT NOT NULL,
  `subtask` VARCHAR(150) NOT NULL,
  `subtask_status` BOOLEAN,
  FOREIGN KEY (task_id) REFERENCES tasks(id) ON DELETE CASCADE
  
);

CREATE table api_data.refresh_tokens (
    `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `user_id` int not null,
    `token` varchar(255),
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);



INSERT INTO api_data.users
values(0,"test","password");



INSERT INTO api_data.tasks
values("1","1","test",'0');


INSERT INTO api_data.refresh_tokens
values("0","1","test");

INSERT INTO api_data.subtasks
values("1","1","test",'0');

