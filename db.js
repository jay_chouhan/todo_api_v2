const Sequelize = require('sequelize');

const db = new Sequelize('api_data', 'root', 'password', {
  host: 'localhost',
  dialect: 'mysql',
});

const testDb = (async () => {
  try {
    await db.authenticate();
    console.log('Connection has been established with database successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
})();

module.exports = db;
