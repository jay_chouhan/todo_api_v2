const jwt = require('jsonwebtoken');

const authorization = (req, res, next) => {
  try {
    const authHeaders = req.headers['authorization'];

    const token = authHeaders.split(' ')[1];

    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    req.user = decoded;
    next();
  } catch (error) {
    res.status(403).json({ msg: 'not authorised' });
  }
};

module.exports = authorization;
