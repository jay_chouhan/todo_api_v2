require('dotenv').config();
const db = require('../db');

const generateToken = require('./generateToken');
const RefreshTokens = require('../models/Refresh-tokens');

const loginUser = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { username, id, password } = user.dataValues;

      const accessTokenPromise = generateToken({ username: username, id: id }, process.env.ACCESS_TOKEN_SECRET, {
        expiresIn: '20s',
      });
      const refreshTokenPromise = generateToken(
        { username: username, id: id },
        process.env.REFRESH_TOKEN_SECRET + password
      );

      const [accessToken, refreshToken] = await Promise.all([accessTokenPromise, refreshTokenPromise]);

      const rows = await RefreshTokens.findAll({
        where: {
          user_id: id,
        },
      });

      if (rows.length === 0) {
        await RefreshTokens.create({
          user_id: id,
          token: refreshToken,
        });
      } else {
        await RefreshTokens.update(
          { token: refreshToken },
          {
            where: {
              user_id: id,
            },
          }
        );
      }

      resolve({ accessToken, refreshToken });
    } catch (err) {
      reject(err);
    }
  });
};

module.exports = loginUser;
