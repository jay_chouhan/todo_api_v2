const express = require('express');
const loginController = require('../controllers/login');
const registerController = require('../controllers/register');
const logoutController = require('../controllers/logout');
const revalidateController = require('../controllers/revalidate');

const router = express.Router();

router.post('/', registerController);

router.post('/login', loginController);

router.post('/logout', logoutController);

router.get('/revalidate-token', revalidateController);

module.exports = router;
