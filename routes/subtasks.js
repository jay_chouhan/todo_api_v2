const express = require('express');
const { getAllSubtasks, getSubtaskById, newSubtask, updateSubtask, deleteSubtask } = require('../controllers/subtasks');
const db = require('../db');
const authorization = require('../middleware/authorization');
const router = express.Router();

router.use(express.json());

router.get('/', authorization, getAllSubtasks);

router.get('/:id', authorization, getSubtaskById);

router.post('/', authorization, newSubtask);

router.patch('/:id', authorization, updateSubtask);

router.delete('/:id', authorization, deleteSubtask);

module.exports = router;
