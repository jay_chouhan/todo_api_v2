const bcrypt = require('bcrypt');
const db = require('../db');
const Users = require('../models/Users');
const login = require('../modules/login');

module.exports = async (req, res) => {
  try {
    const { username, password } = req.body;

    const user = await Users.findOne({
      where: {
        username: username,
      },
    });

    if (user === null) {
      return res.status(401).json({ msg: 'No user found' });
    }
    const result = await bcrypt.compare(password, user.dataValues.password);

    if (!result) {
      return res.status(401).json({ msg: 'Password incorrect' });
    }

    const { accessToken, refreshToken } = await login(user);

    res.json({ msg: 'login success', accessToken, refreshToken });
  } catch (error) {
    console.error(error);
  }
};
