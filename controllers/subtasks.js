const db = require('../db');
const Subtasks = require('../models/Subtasks');
const Tasks = require('../models/Tasks');

const getAllSubtasks = async (req, res) => {
  try {
    const allSubtasks = await Subtasks.findAll();

    res.status(200).json(allSubtasks);
  } catch (err) {
    res.status(500).json({ msg: 'server error' });
  }
};

const getSubtaskById = async (req, res) => {
  try {
    const id = req.params.id;

    const singleSubtask = await Subtasks.findOne({
      where: {
        id,
      },
    });
    if (singleSubtask === null) {
      return res.status(404).json({ msg: 'not found' });
    }

    res.status(200).json(singleSubtask);
  } catch (err) {
    res.status(500).json({ msg: 'server error' });
  }
};

const newSubtask = async (req, res) => {
  try {
    const task_id = req.body.taskId;
    const subtask = req.body.subtask;

    if (!task_id || !subtask) {
      return res.status(400).json({ msg: 'bad request' });
    }

    const verifyTask = await Tasks.findOne({ where: { id: task_id } });
    if (!verifyTask) {
      return res.status(404).json({ msg: 'task not found' });
    }
    const { user_id } = verifyTask;
    if (user_id === req.user.id) {
      const newsubTask = await Subtasks.create({
        task_id,
        subtask,
        subtask_status: 0,
      });

      res.status(201).json(newsubTask.dataValues);
    } else {
      res.status(401).json({ msg: 'not authorised' });
    }
  } catch (err) {
    res.status(400).json({ msg: 'bad request' });
  }
};

const updateSubtask = async (req, res, next) => {
  try {
    const id = req.params.id;
    const subtask_status = req.body.status;
    const subtask = req.body.subtask;

    const subtaskDetails = await Subtasks.findOne({ where: { id } });

    if (!subtaskDetails) {
      return res.status(404).json({ msg: 'not found' });
    }
    const { task_id } = subtaskDetails.dataValues;

    const taskdetails = await Tasks.findOne({
      where: { id: task_id },
    });
    const { user_id } = taskdetails.dataValues;
    if (user_id === req.user.id) {
      if (!subtask) {
        const updateTask = await Subtasks.update({ subtask_status }, { where: { id } });
      }
      if (!subtask_status) {
        const updateTask = await Subtasks.update({ subtask }, { where: { id } });
      }
      if (subtask && subtask_status) {
        const updateTask = await Subtasks.update({ subtask, subtask_status }, { where: { id } });
      }

      res.status(201).json({ status: 'task updated' });
    } else {
      res.status(401).json({ msg: 'not authorised' });
    }
  } catch (err) {
    res.status(400).json({ msg: 'bad request' });
  }
};

const deleteSubtask = async (req, res) => {
  try {
    const id = req.params.id;

    const subtask = await Subtasks.findOne({
      where: { id },
    });

    if (!subtask) {
      return res.status(404).json({ msg: 'not found' });
    }
    const { task_id } = subtask.dataValues;

    const taskdetails = await Tasks.findOne({
      where: { id: task_id },
    });
    const { user_id } = taskdetails.dataValues;

    if (user_id === req.user.id) {
      const deletesubTask = await Subtasks.destroy({ where: { id } });
      res.status(202).json({ msg: 'success' });
    } else {
      res.status(401).json({ msg: 'not authorised' });
    }
  } catch (err) {
    console.error(err);

    res.status(500).json({ msg: 'server error' });
  }
};

module.exports = { getAllSubtasks, getSubtaskById, newSubtask, updateSubtask, deleteSubtask };
