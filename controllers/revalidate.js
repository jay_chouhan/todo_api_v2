const jwt = require('jsonwebtoken');
const generateToken = require('../modules/generateToken');
const db = require('../db');
const RefreshTokens = require('../models/Refresh-tokens');
const Users = require('../models/Users');

module.exports = async (req, res) => {
  try {
    const refreshHeader = req.headers['x-refresh-token'];
    const refreshToken = refreshHeader.split(' ')[1];
    const { username, id } = jwt.decode(refreshToken);

    const refreshQuery = await RefreshTokens.findOne({
      where: {
        user_id: id,
      },
    });

    if (refreshQuery === null) {
      return res.status(401).json({ msg: `you're not logged in` });
    }
    const refreshData = refreshQuery.dataValues;
    if (refreshData.token === refreshToken) {
      const user = await Users.findOne({
        where: {
          id: id,
        },
      });

      const { password } = user.dataValues;
      jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET + password, async (error, decoded) => {
        if (error) {
          console.log(error);
        } else {
          const accessToken = await generateToken({ username, id }, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: 20,
          });
          res.json({ accessToken });
        }
      });
    } else {
      throw new Error('Invalid refresh token');
    }
  } catch (error) {
    console.error(error);
    res.status(400).json({ msg: 'Invalid token' });
  }
};
