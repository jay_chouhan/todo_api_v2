const db = require('../db');
const Tasks = require('../models/Tasks');

const getAllTasks = async (req, res) => {
  try {
    const allTasks = await Tasks.findAll();

    res.status(200).json(allTasks);
  } catch (err) {
    res.status(500).json({ msg: 'server error' });
  }
};

const getTaskById = async (req, res) => {
  try {
    const id = req.params.id;

    const singleTask = await Tasks.findOne({
      where: {
        id,
      },
    });
    if (singleTask === null) {
      return res.status(404).json({ msg: 'not found' });
    }

    res.status(200).json(singleTask);
  } catch (err) {
    res.status(500).json({ msg: 'server error' });
  }
};

const newTask = async (req, res) => {
  try {
    const user_id = req.user.id;
    const task = req.body.task;

    if (!task) {
      return res.status(400).json({ msg: 'bad request' });
    }

    const newTask = await Tasks.create({
      task,
      user_id,
      task_status: 0,
    });

    res.status(201).json(newTask.dataValues);
  } catch (err) {
    res.status(500).json({ msg: 'server error' });
  }
};

const updateTask = async (req, res, next) => {
  try {
    const id = req.params.id;
    const task = req.body.task;
    const task_status = req.body.status;

    const taskdetails = await Tasks.findOne({
      where: { id },
      attributes: ['user_id'],
    });

    if (taskdetails === null) {
      return res.status(404).json({ msg: 'not found' });
    }
    const { user_id } = taskdetails.dataValues;

    if (user_id === req.user.id) {
      if (!task) {
        const updateTask = await Tasks.update({ task_status }, { where: { id } });
      }
      if (!task_status) {
        const updateTask = await Tasks.update({ task }, { where: { id } });
      }
      if (task && task_status) {
        const updateTask = await Tasks.update({ task, task_status }, { where: { id } });
      }

      res.status(201).json({ status: 'task updated' });
    } else {
      res.status(401).json({ msg: 'not authorised' });
    }
  } catch (err) {
    res.status(500).json({ msg: 'server error' });
  }
};

const deleteTask = async (req, res) => {
  try {
    const id = req.params.id;

    const task = await Tasks.findOne({
      where: { id },
      attributes: ['user_id'],
    });

    if (task === null) {
      return res.status(404).json({ msg: 'not found' });
    }
    const { user_id } = task.dataValues;

    if (user_id === req.user.id) {
      const deleteTask = await Tasks.destroy({ where: { id } });
      res.status(202).json({ msg: 'success' });
    } else {
      res.status(401).json({ msg: 'not authorised' });
    }
  } catch (err) {
    res.status(500).json({ msg: 'server error' });
  }
};

module.exports = { getAllTasks, getTaskById, newTask, updateTask, deleteTask };
