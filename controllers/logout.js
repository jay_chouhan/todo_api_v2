const jwt = require('jsonwebtoken');
const db = require('../db');
const RefreshTokens = require('../models/Refresh-tokens');

module.exports = async (req, res) => {
  try {
    const refreshHeader = req.headers['x-refresh-token'];
    const refreshToken = refreshHeader.split(' ')[1];
    const { id } = jwt.decode(refreshToken);

    const deleteToken = await RefreshTokens.destroy({
      where: {
        token: refreshToken,
      },
    });
    if (deleteToken) {
      res.json({ msg: 'Logout Success' });
    } else {
      throw new Error('No Refresh Token in DB');
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({ msg: 'Invalid Token' });
  }
};
