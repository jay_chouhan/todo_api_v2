const bcrypt = require('bcrypt');
const db = require('../db');
const login = require('../modules/login');
const User = require('../models/Users');

module.exports = async (req, res) => {
  try {
    const { username, password } = req.body;
    const hash = await bcrypt.hash(password, 10);

    const user = await User.create(
      {
        username,
        password: hash,
      },
      { fields: ['username', 'password'] }
    );

    const { accessToken, refreshToken } = await login(user);

    return res.status(201).json({ accessToken, refreshToken });
  } catch (err) {
    console.error(err);
    return res.status(400).json({ msg: `username has already been taken` });
  }
};
